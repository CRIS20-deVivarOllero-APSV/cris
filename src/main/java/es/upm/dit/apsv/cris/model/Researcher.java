package es.upm.dit.apsv.cris.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;





@Entity

public class Researcher implements Serializable {
	private static final long serialVersionUID =1L;
	
	
	@Id
	private String id;
	private String name;
	private String lastname;
	private String password;
	private String scopusURL;
	private String email;
	public String getEmail() {
		return email;
	}
	public Researcher() {
		super();

	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getScopusURL() {
		return scopusURL;
	}
	public void setScopusURL(String scopusURL) {
		this.scopusURL = scopusURL;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((lastname == null) ? 0 : lastname.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((scopusURL == null) ? 0 : scopusURL.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Researcher other = (Researcher) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (lastname == null) {
			if (other.lastname != null)
				return false;
		} else if (!lastname.equals(other.lastname))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (scopusURL == null) {
			if (other.scopusURL != null)
				return false;
		} else if (!scopusURL.equals(other.scopusURL))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Researcher [id=" + id + ", name=" + name + ", lastname=" + lastname + ", password=" + password
				+ ", scopusURL=" + scopusURL + "]";
	}
	public void setEmail(String string) {
		// TODO Auto-generated method stub

	}



}

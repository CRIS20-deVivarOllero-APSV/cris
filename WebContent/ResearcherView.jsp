<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ include file="Header.jsp"%>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Researcher View</title>
</head>
<body>

	<table border="1">
		<tr>
			<th>Id</th>
			<th>Name</th>
			<th>Last name</th>
			<th>Password</th>
			<th>Email</th>
			<th>URL</th>
		</tr>


		<tr>
			<td>${request.id}</td>
			<td>${response.id}</td>
			<td>${ri.id}</td>
			<td>${ri.name}</td>
			<td>${ri.lastname}</td>
			<td>${ri.password}</td>
			<td>${ri.email}</td>
			<td>${ri.scopusURL}</td>
		</tr>

	</table>


</body>
</html>